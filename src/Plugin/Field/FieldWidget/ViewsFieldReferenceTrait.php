<?php

namespace Drupal\views_field_reference\Plugin\Field\FieldWidget;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\ViewsPluginManager;
use Drupal\views\Views;
use Drupal\views_field_reference\ViewsDisplayListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ViewsFieldReferenceTrait
 * @package Drupal\views_field_reference\Plugin\Field\FieldWidget
 */
trait ViewsFieldReferenceTrait {

  protected $viewsDisplayList;

  // TODO can the exposed form plugin be of any use. Or is this to specific.

  protected $exposeForm;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings, array $third_party_settings,
                              ViewsDisplayListInterface $views_display_list,
                              ViewsPluginManager $exposed_form) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition,
      $settings, $third_party_settings);

    $this->viewsDisplayList = $views_display_list;

    $this->exposeForm = $exposed_form;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('views_field_reference.viewsdisplaylist'),
      $container->get('plugin.manager.views.exposed_form')
    );
  }

  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param int $delta
   * @param array $element
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   */
  public function fieldElement($element, FieldItemListInterface $items, $delta) {

    switch ($element['target_id']['#type']) {
      case 'select':
        $test = array('!value' => '');
        $event = 'change';
        break;

      default:
        $test = array('filled' => TRUE);
        $event = 'viewsreference-select';
        break;
    }

    $field_name = $items->getName();
    $name = $field_name . '[' . $delta . '][target_id]';

    $default_value = isset($items[$delta]->getValue()['display_id']) ? $items[$delta]->getValue()['display_id'] : '';

    if ($default_value == '') {
      $options = $this->viewsDisplayList->getAllViewsDisplayIds();
    }
    else {
      $options = $this->viewsDisplayList->getViewDisplayIds($items[$delta]->getValue()['target_id']);
    }

    $class = $this->createClassName($element['target_id']['#field_parents']);
    $classDisplay = $class . $field_name . '-' . $delta . '-display-id';

    $element['target_id']['#target_type'] = 'view';
    $element['target_id']['#ajax'] = array(
      'callback' => array($this, 'loadDisplayIds'),
      'event' => $event,
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Getting display Ids...'),
      ),
    );

    $element['title'] = array(
      '#title' => 'Include View Title',
      '#type' => 'checkbox',
      '#default_value' => isset($items[$delta]->getValue()['title']) ? $items[$delta]->getValue()['title'] : '',
      '#weight' => 10,
      '#states' => array(
        'visible' => array(
          ':input[name="' . $name . '"]' => $test,
        ),
      ),
    );

    $element['display_id'] = array(
      '#title' => 'Display Id',
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $default_value,
      '#weight' => 10,
      '#attributes' => array(
        'class' => array(
          $classDisplay
        )
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="' . $name . '"]' => $test,
        ),
      ),
    );

    $element['expose_filter_form'] = array(
      '#title' => 'Hide View Filter Form on Page',
      '#type' => 'checkbox',
      '#default_value' => isset($items[$delta]->getValue()['expose_filter_form']) ? $items[$delta]->getValue()['expose_filter_form'] : '',
      '#weight' => 14,
      '#delta' => $delta,
      '#states' => array(
        'visible' => array(
          ':input[name="' . $name . '"]' => $test,
        ),
      ),
    );

    $element['argument'] = array(
      '#title' => 'Argument',
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->getValue()['argument']) ? $items[$delta]->getValue()['argument'] : '',
      '#weight' => 20,
      '#states' => array(
        'visible' => array(
          ':input[name="' . $name . '"]' => $test,
        ),
      ),
    );

    return $element;
  }

  /**
   *  AJAX function to get display IDs for a particular View
   */
  public function loadDisplayIds(array &$form, FormStateInterface $form_state) {

    $trigger = $form_state->getTriggeringElement();
    $delta = $trigger['#delta'];
    $field_name = $trigger['#parents'][0];
    $values = $form_state->getValues();
    $parents = $trigger['#parents'];
    array_shift($parents);

    // Get the value for the target id of the View.
    switch ($trigger['#type']) {
      case 'select':
        $entity_id = $this->viewsDisplayList->getSelectEntityId($values[$field_name], $parents);
        break;

      default:
        $entity_id = $this->viewsDisplayList->getEntityId($values[$field_name], $parents);
    }

    if (count($parents) > 2) {
      $field_name = $parents[count($parents) - 3];
    }

    // Obtain the display ids for the given View.
    $options = $this->viewsDisplayList->getViewDisplayIds($entity_id);
    // We recreate the same unique class as in the parent function.
    $class = $this->createClassName($trigger['#field_parents']);
    $element_class = '.' . $class . $field_name . '-' . $delta .
      '-display-id';

    // Construct the html.
    $html = '<optgroup>';
    foreach ($options as $key => $option) {
      $html .= '<option value="' . $key . '">' . $option . '</option>';
    }
    $html .= '</optgroup>';
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand($element_class, render($html)));
    return $response;
  }

  /**
   * @param $fieldParents
   * @return string
   */
  private function createClassName($fieldParents) {

    $class = !empty($fieldParents) ? implode('-', $fieldParents) . '-' : '';

    return $class;
  }

}