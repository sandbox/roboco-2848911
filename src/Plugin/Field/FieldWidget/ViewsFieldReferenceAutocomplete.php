<?php

namespace Drupal\views_field_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'views_field_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "views_field_reference_autocomplete",
 *   label = @Translation("Views field reference autocomplete"),
 *   field_types = {
 *     "views_field_reference"
 *   }
 * )
 */
class ViewsFieldReferenceAutocomplete extends EntityReferenceAutocompleteWidget {

  use ViewsFieldReferenceTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element = $this->fieldElement($element, $items, $delta);

    return $element;
  }

}
