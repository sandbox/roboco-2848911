<?php

namespace Drupal\views_field_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'views_field_reference_select' widget.
 *
 * @FieldWidget(
 *   id = "views_field_reference_select",
 *   label = @Translation("Views field reference select"),
 *   field_types = {
 *     "views_field_reference"
 *   }
 * )
 */
class ViewsFieldReferenceSelectWidget extends OptionsSelectWidget implements ContainerFactoryPluginInterface {

  use ViewsFieldReferenceTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items,
                              $delta,
                              array $element,
                              array &$form,
                              FormStateInterface $form_state) {

    $select_element['target_id'] = parent::formElement($items, $delta, $element, $form, $form_state);

    $select_element = $this->fieldElement($select_element, $items, $delta);
    $select_element['target_id']['#multiple'] = FALSE;
    if (!$this->isDefaultValueWidget($form_state)) {
      $selected_views = $items->getSetting('available_views');
      $selected_views = array_diff($selected_views, array("0"));
      if (count($selected_views) >= 1) {
        $first_option = array("- None -");
        $select_element['target_id']['#options'] = array_merge($first_option, $selected_views);
      } else {
        // $select_element['target_id']['#options'] = array_shift($select_element['target_id']['#options']);.
        $select_element['target_id']['#empty_option'] = '- None -';
      }
    }

    return $select_element;
  }
  
}
