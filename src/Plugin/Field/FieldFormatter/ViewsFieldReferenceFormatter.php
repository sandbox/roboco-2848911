<?php

namespace Drupal\views_field_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'views_field_reference_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "views_field_reference_formatter",
 *   label = @Translation("Views field reference formatter"),
 *   field_types = {
 *     "views_field_reference"
 *   }
 * )
 */
class ViewsFieldReferenceFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    $options['render_view'] = TRUE;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    // We may decide on alternatives to rendering the view so get settings established
    $form['render_view'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Render View'),
      '#default_value' => $this->getSetting('render_view'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();

    $summary[] = t('Render View: @view', array('@view' => $settings['render_view'] ? 'TRUE' : 'FALSE'));
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $view_name = $item->getValue()['target_id'];
      $title = $item->getValue()['title'];
      $display_id = $item->getValue()['display_id'];
      $argument = $item->getValue()['argument'];
      $view = \Drupal\views\Views::getView($view_name);
      // Someone may have deleted the View
      if (!is_object($view)) {
        continue;
      }
      $view->setDisplay($display_id);
      $view->build($display_id);
      $view->execute($display_id);
      // We find the result to avoid rendering an empty view
      $result = $view->result;

      if ($title) {
        $title = $view->getTitle();
        $title_render_array = array(
          '#markup' => '<div class="viewsreference-title">' . t('@title', ['@title'=> $title]) . '</div>'
        );
      }

      if ($this->getSetting('render_view')) {
        if ($title && !empty($result)) {
          $elements[$delta]['title'] = $title_render_array;
        }
        $elements[$delta]['contents'] = views_embed_view($view_name, $display_id, $argument);
      }

    }

    return $elements;
  }

}
