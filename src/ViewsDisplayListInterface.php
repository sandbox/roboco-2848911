<?php

namespace Drupal\views_field_reference;

/**
 * Interface ViewsDisplayListInterface.
 *
 * @package Drupal\views_field_reference
 */
interface ViewsDisplayListInterface {

  /**
   * @param mixed $values
   */
  public function setValues($values);

  /**
   * @param mixed $parents
   */
  public function setParents($parents);

  /**
   * @param mixed $entity_id
   */
  public function setEntityId($entityId);

  /**
   * @return array|bool
   */
  public function getEntityId($values, $parents);

  /**
   * @return array|bool
   */
  public function getSelectEntityId($values, $parents);

  /**
   * @return array|bool
   */
  public function getAllViewsDisplayIds();

  /**
   * @return array|bool
   */
  public function getViewDisplayIds($entityId);


}
