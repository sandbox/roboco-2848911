<?php

namespace Drupal\views_field_reference;

use Drupal\views\Views;

/**
 * Class ViewsDisplayList.
 *
 * @package Drupal\views_field_reference
 */
class ViewsDisplayList implements ViewsDisplayListInterface {

  private $values;

  private $parents;

  private $entityId;

  /**
   * @param mixed $values
   */
  public function setValues($values) {
    $this->values = $values;
  }

  /**
   * @param mixed $parents
   */
  public function setParents($parents) {
    $this->parents = $parents;
  }

  /**
   * @param mixed $entity_id
   */
  public function setEntityId($entityId) {
    $this->entityId = $entityId;
  }

  /**
   * @return array|bool
   */
  public function getEntityId($values, $parents) {
    $this->setValues($values);
    $this->setParents($parents);
    return $this->entityId();
  }

  /**
   * @return array|bool
   */
  public function getSelectEntityId($values, $parents) {
    $this->setValues($values);
    $this->setParents($parents);
    return $this->selectEntityId();
  }

  /**
   * @return array|bool
   */
  public function getAllViewsDisplayIds() {
    return $this->allViewsDisplayIds();
  }

  /**
   * @return array|bool
   */
  public function getViewDisplayIds($entityId) {
    $this->setEntityId($entityId);
    return $this->viewDisplayIds();
  }

  /**
   * Helper function to get the current entity_id value from the values array.
   * Based on parent array
   *
   * @return array|bool
   */
  private function entityId() {
    $key = array_shift($this->parents);
    $this->setValues($this->values[$key]);
    if (is_array($this->values)) {
      $this->setValues($this->values);
      $this->setParents($this->parents);
      $this->values = $this->getEntityId($this->values, $this->parents);
    }
    return $this->values;
  }

  /**
   * Helper function to get the current entity_id value from the values array.
   * Based on parent array for select element.
   *
   * Select adds an extra array level.
   *
   * @return array|bool
   */
  private function selectEntityId() {

    $_parents = $this->parents;
    $key = array_shift($_parents);
    // Fix for paragraphs.
    if (count($this->parents) > 2) {
      $this->setParents(array_slice($this->parents, 3));
      $this->setValues($this->values[$key]);
      $key = array_shift($_parents);
      $this->setValues($this->values[$key]);
      $key = array_shift($_parents);
      $this->setValues($this->values[$key]);
      $key = array_shift($_parents);
    }
    $this->setValues($this->values[$key]);
    $key = array_shift($_parents);
    return $this->getEntityId($this->values[$key], $this->parents);
    
  }

  /**
   * Helper function to get all display ids.
   */
  private function allViewsDisplayIds() {
    $views = Views::getAllViews();
    $options = array();
    foreach ($views as $view) {
      foreach ($view->get('display') as $display) {
        $options[$display['id']] = $display['display_title'];
      }
    }
    return $options;
  }

  /**
   * @return array
   */
  private function viewDisplayIds() {
    $views = Views::getAllViews();
    $options = array();
    foreach ($views as $view) {
      if ($view->get('id') == $this->entityId) {
        foreach ($view->get('display') as $display) {
          $options[$display['id']] = $display['display_title'];
        }
      }
    }
    return $options;
  }

}
